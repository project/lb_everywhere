<?php

namespace Drupal\lb_everywhere\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines an AJAX command that exits layout mode.
 *
 * @ingroup ajax
 */
class ExitLayoutModeCommand implements CommandInterface {

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'exitLayoutMode',
    ];
  }

}
